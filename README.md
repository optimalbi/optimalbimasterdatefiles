# Instructions on using OptimalBI's master date file

## Context
Building a date dimension can be tough because users of your data want every detail of each date (regional holidays, days until Easter, Fridays since Ramadan) immediately!

At OptimalBI we believe trying to deliver everything at once makes success less likely, so we deliver small slices of work that prove themselves early. As long as we use a framework that accepts and manages change and improvement over time, we can move faster and faster to meet user requirements, without breaking what we've already done.

## Solution
Our solution to building a date dimension is to start with the absolute minimum: a list of dates from an arbitrary point in the past to a high-end date of 31 December 9999 (we assume most data warehouses will be replaced before then). Once we've established all the dates we want to use, adding further details about these dates is simple.

In general, the way you build a date dimension using our master files is the same way you build any data: load it, govern its transformation, present to users as they want to see it. We recommend you use the Optimal Data Engine (http://ode.ninja) for everything, but then we're biased.

## Instructions
Download the file "ode_dates_master_data_file_HUB.csv" in the public directory above. This is a plain-text list of all the dates from 0001-01-01 through 9999-12-31 (YYYY-MM-DD), and the components of those dates as numbers.

"Where are my day names?", "where I my holidays?" I hear you ask. Every additional detail about a given date comes from a separate file.

Why? So that we build up the date dimension iteratively, starting with something easy and adding hard things later. It's also good practice for using the data vault layers within Optimal Data Engine (http://ode.ninja).

Every time we discover some new details about a given set of dates, we add them as a separate file with SAT in the name to the public directory above. There are only as many rows as we have data about these new details: if we only know Moroccan national public holidays from 1970 up to next year, we only provide a subset of all the dates in the HUB file. This is by design.

To understand our approach, download the file ode_dates_master_data_file_SAT_english_names.csv and note that it has all the same dates as the HUB file, and English names of months and dates, because those are known for all the dates in the HUB.

Now, whatever your tool:
- orchestrate it to load these two separately as flat files
- subset the date range to your needs (and platform limitations) before converting to date format
- join them on their shared detail (date)
- plumb them through your data layers
- present to end-users as the date dimension they asked for.

Once you've achieved all that, you've proven to yourself and your customers that you can do the same with any other detail about dates they could desire!

